<?php
if ($include_config == "a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s") {
    include('admin/modules/config/pre-head.php');
    include('admin/modules/config/post-body.php');

    cabecera('Relato - Administracion de Trendings');
    ?>

    <div class="panel panel-default">
        <br/>
        <div class="panel-body">
            <script type="text/javascript" src="admin/modules/trends/programing.js"></script>
            <center>
                <h2>
                    Lista de Trendings.
                </h2>
            </center>
            <div class="table-responsive">
                <div id="txtHint"></div>
            </div>
            <br>
            <form>
                <fieldset>
                    <legend>Ingreso de trending manual.(todos los parametros son obligatorios)</legend>
                    <label for="titulo">Titulo</label>
                    <input type="text" name="titulo" id="titulo"/>
                    <label for="tema">Tema</label>
                    <input type="text" name="tema" id="tema"/>
                    <label for="dom">Dominio</label>
                    <input type="text" name="dom" id="dom"/>
                    <label for="url">url</label>
                    <input type="text" name="url" id="url"/>
                    <label for="ini_date">Inicia en... (yyyy-mm-dd hh:mm:ss)</label>
                    <input type="text" name="ini_date" id="ini_date"/>
                    <label for="end_date">Termina en... (yyyy-mm-dd hh:mm:ss)</label>
                    <input type="text" name="end_date" id="end_date"/>
                </fieldset>
            </form>
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-left">
                    <div class="previous">
                        <button  data-role="button" onclick="ingresar()">
                            Ingresar Nuevo Trending
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 text-right">   
                    <div class="next">
                        <button  data-role="button" onclick="notificar()">
                            Notificar Trendings.
                        </button>
                    </div>
                </div>
            </div>
            <div id="dialog-confirm" title="Aceptar Trendings" style='display:none;background-color: white'>
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Esta seguro que desea Activar/Desactivar este trending?</p>
            </div>
        </div>
    </div>
    <?php
    pie();
}