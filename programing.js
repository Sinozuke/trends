var idpress = "";
var tipo = 0;
window.onload = function () {
    $.post("admin/modules/trends/getData.php",
            {
                getD: "trends"
            },
            function (data) {
                document.getElementById("txtHint").innerHTML = data;
            }
    );
};
$(function () {

    $("#dialog-confirm").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Cambiar": function () {
                $.post("admin/modules/trends/Actions.php",
                        {
                            type: "update",
                            p1: idpress,
                            p2: tipo
                        },
                        function (data) {
                            alert(data);
                        }
                );
                $.post("admin/modules/trends/getData.php",
                        {
                            getD: "trends"
                        },
                        function (data) {
                            document.getElementById("txtHint").innerHTML = "";
                            ;
                            document.getElementById("txtHint").innerHTML = data;
                        }
                );
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });

});

function ingresar(){    
    var error = 0;
    if(document.getElementById("titulo").value===""){
        alert("El titulo esta Vacio!");
        error = 1;
    }
    if(document.getElementById("tema").value===""){
        alert("El tema esta Vacio!");
        error = 1;
    }
    if(document.getElementById("dom").value===""){
        alert("El dominio esta Vacio!");
        error = 1;
    }
    if(document.getElementById("url").value===""){
        alert("El url esta Vacio!");
        error = 1;
    }
    if(document.getElementById("ini_date").value===""){
        alert("La fecha de inicio esta vacia!");
        error = 1;
    }
    if(document.getElementById("end_date").value===""){
        alert("La fecha de fin esta vacia!");
        error = 1;
    }
    if (error===1){
        return;
    }
    $.post("admin/modules/trends/Actions.php",
            {
                type: "agregar",
                p1: document.getElementById("titulo").value,
                p2: document.getElementById("tema").value,
                p3: document.getElementById("dom").value,
                p4: document.getElementById("url").value,
                p5: document.getElementById("ini_date").value,
                p6: document.getElementById("end_date").value
            },
            function (data) {
                alert(data);
            }
    );
    $.post("admin/modules/trends/getData.php",
            {
                getD: "trends"
            },
            function (data) {
                document.getElementById("txtHint").innerHTML = "";
                ;
                document.getElementById("txtHint").innerHTML = data;
            }
    );
}

function notificar(){
    $.post("admin/modules/trends/Actions.php",
            {
                type: "notificar"
            },
            function (data) {
                alert(data);
            }
    );
}

function checar(id, tipo2) {
    idpress = id;
    tipo = tipo2;
    $.post("admin/modules/trends/Actions.php",
            {
                type: "update",
                p1: idpress,
                p2: tipo
            },
            function (data) {
                alert(data);
            }
    );
    $.post("admin/modules/trends/getData.php",
            {
                getD: "trends"
            },
            function (data) {
                document.getElementById("txtHint").innerHTML = "";
                ;
                document.getElementById("txtHint").innerHTML = data;
            }
    );
}