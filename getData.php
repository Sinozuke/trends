<?php

if (!filter_has_var(INPUT_POST, "getD")) {
    die("<h1>401 - Unauthorized: Access is denied due to invalid credentials</h1>");
}

include '../config/DATA.php';

$con = mysqli_connect(HOST, USER, PASS, DB);
mysqli_set_charset($con, 'utf8');
$q = filter_input(INPUT_POST, "getD");

if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
} else {

    switch ($q) {
        case "trends":
            $sql = "SELECT * FROM `trend` WHERE `status` > 0";
            $result = mysqli_query($con, $sql);

            echo "<table class=\"table table-striped table-hover\">
                <thead>
                <tr>
                <th>Trending</th>
                <th COLSPAN=\"2\">Accion</th>
                </tr>
                </thead>";
            echo "<tbody>";
            while ($row = mysqli_fetch_array($result)) {
                echo "<tr>";
                echo "<td>Tema: " . $row['topic'];
                echo "<br>Titulo: " . $row['title'];
                echo "<br>url: " . $row['url'] . "</td>";
                switch($row['status']){
                    case 1:
                        echo "<td>Inactivo</td>";
                        echo "<td><button id=\"" . $row['id'] . "\" data-role=\"button\" onclick=\"checar(this.id,2)\">Activar</button></td>";
                        break;
                    case 2:
                        echo "<td>Activo</td>";
                        echo "<td><button id=\"" . $row['id'] . "\" data-role=\"button\" onclick=\"checar(this.id,1)\">Desactivar</button></td>";
                        break;
                }
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
            break;
    }
    mysqli_close($con);
}