<?php

include '../config/DATA.php';

if (!filter_has_var(INPUT_POST, "type")) {
    die("<h1>401 - Unauthorized: Access is denied due to invalid credentials</h1>");
}

function mandar($para, $asunto, $mensaje, $de) {

    $headers = 'From: ' . $de . "\r\n" .
            'Reply-To: chewysaguilar@hotmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

    return mail($para, $asunto, $mensaje, $headers);
}

function enviar_correo($admin, $nuevos) {

    $mensaje = "Los trendings nuevos son:<br>";

    foreach ($nuevos as $nuevo) {
        $mensaje = $mensaje . "TEMA: " . $nuevo['topic'] . "<br>";
        $mensaje = $mensaje . "TITULO: " . $nuevo['title'] . "<br>";
        $mensaje = $mensaje . "URL: " . $nuevo['url'] . "<br>";
        $mensaje = $mensaje . "----------------------------------------------------<br><br>";
    }

    if (mandar($admin['email'], "Actualizacion de Trendings", $mensaje, "www.relato.gt")) {
        echo "Correo enviado a: \"" . $admin['email'] . "\"<br>Mensaje:<br>" . $mensaje;
    } else {
        echo "Fallo al Enviar Correo.";
    }
}

function getTrending($con) {
    $regresa = array();
    $query = 'SELECT * FROM `trend` WHERE status = 2';
    $result = mysqli_query($con, $query);
    $position = 0;
    while ($row = mysqli_fetch_array($result)) {
        $regresa[$position] = $row;
        $position++;
    }
    return $regresa;
}

function getEditores($con) {
    $regresa = array();
    $query = 'SELECT fname,lname,email FROM `users` WHERE role = 3';
    $result = mysqli_query($con, $query);
    $position = 0;
    while ($row = mysqli_fetch_array($result)) {
        $regresa[$position] = $row;
        $position++;
    }
    return $regresa;
}

function informar($con) {

    $editores = getEditores($con);
    $nuevos = getTrending($con);

    if (count($nuevos) != 0) {
        foreach ($editores as $editor) {
            enviar_correo($editor, $nuevos);
        }
    }
}

$con = mysqli_connect(HOST, USER, PASS, DB);

if (!$con) {
    echo('Fallo Temporal, Intentelo mas tarde.\nError: ' . mysqli_error($con));
} else {
    $q = filter_input(INPUT_POST, 'type');
    $sql = "";
    $p1 = filter_input(INPUT_POST, 'p1');
    $p2 = filter_input(INPUT_POST, 'p2');
    $p3 = filter_input(INPUT_POST, 'p3');
    $p4 = filter_input(INPUT_POST, 'p4');
    $p5 = filter_input(INPUT_POST, 'p5');
    $p6 = filter_input(INPUT_POST, 'p6');

    switch ($q) {
        case "update":
            $sql = "UPDATE `trend` SET `status`=" . $p2 . " WHERE `id` = " . $p1 . " ;";
            $result = mysqli_query($con, $sql);
            if ($result) {
                echo "Cambio con exito!";
            } else {
                echo "Fallo al Cambio, intentelo mas tarde.";
            }
            break;
        case "notificar":
            informar($con);
            break;
        case "agregar":
            $sql = "INSERT INTO `trend` (`id`, `topic`, `title`, `dom`, `url`, `ini_date`, `end_date`, `status`) VALUES (NULL, '" . $p2 . "', '" . $p1 . "', '$p3', '" . $p4 . "', '" . $p5 . "', '" . $p6 . "', '2');";
            $result = mysqli_query($con, $sql);
            if ($result) {
                echo "Insercion con exito!";
            } else {
                echo "Fallo al Insertar, intentelo mas tarde.";
            }
            break;
    }
    mysqli_close($con);
}